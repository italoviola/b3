document.addEventListener("DOMContentLoaded", function(event) { 

    //pega todos os tables
    let tables = document.querySelectorAll('.table');
    setPrevNext();

    function setPrevNext() {
        let index;
        for (i = 0; tables.length > i; i++) {
            if (tables[i].classList.contains('active')) {
                index = i;
                let $next = tables[i].querySelector('.title-container-right');    
                let $prev = tables[i].querySelector('.title-container-left');
                $next.onclick = function(){
                    tableChange(index, true);
                }
                $prev.onclick = function(){
                    tableChange(index, false);
                }
            }
        }
    }

    function tableChange(index, boolNext) {
        tables[index].classList.remove('active');
        if (boolNext) {
            index += 1;
            if (index > tables.length - 1) {
                index = 0;
            }
        } else {
            index -= 1;
            if (index < 0) {
                index = tables.length - 1;
            }
        }
        tables[index].classList.add('active');
        setPrevNext()
    }

});